package net.lukepchambers.justworlds

import net.lukepchambers.justworlds.util.SenderType
import net.lukepchambers.justworlds.util.msg
import org.bukkit.command.CommandSender

abstract class JWCommand {
    abstract val name: String
    abstract val description: String
    abstract val usage: String
    abstract val argRange: IntRange
    abstract val senderType: SenderType

    abstract fun runCommand(sender: CommandSender, args: List<String>): Boolean
    // abstract fun tabComplete(sender: CommandSender, args: List<String>): List<String>

    fun invalidArgs(sender: CommandSender) {
        sender.msg("&cInvalid arguments. The correct usage is &e/jw $name $usage&c.")
    }
}