package net.lukepchambers.justworlds.util

import net.md_5.bungee.api.ChatColor
import org.bukkit.command.CommandSender

fun CommandSender.msg(string: String) {
    val prefix = "&8(&b!&8) &3&l»&r &f"
    val message = ChatColor.translateAlternateColorCodes('&', "$prefix$string")

    sendMessage(message)
}