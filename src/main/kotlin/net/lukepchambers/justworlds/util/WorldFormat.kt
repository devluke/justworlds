package net.lukepchambers.justworlds.util

enum class WorldFormat(val formatName: String) {
    JUSTWORLDS("JustWorlds"),
    JUSTWORLDS_OLD("JustWorlds (old)"),
    OTHER("other")
}