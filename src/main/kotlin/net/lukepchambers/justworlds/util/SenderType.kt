package net.lukepchambers.justworlds.util

import org.bukkit.command.ConsoleCommandSender
import org.bukkit.entity.Player
import kotlin.reflect.KClass

enum class SenderType(val typeName: String, val typeClass: KClass<*>) {
    CONSOLE("console", ConsoleCommandSender::class),
    PLAYER("player", Player::class),
    ANY("any", Any::class)
}