package net.lukepchambers.justworlds.util

import org.bukkit.Location
import org.bukkit.Server
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File

private val sep = File.separator

fun worldExists(name: String, server: Server): Boolean {
    val world = server.getWorld(name)
    return world != null
}

fun getWorlds(loaded: Boolean, server: Server): Map<String, WorldFormat> {
    val worlds = mutableMapOf<String, WorldFormat>()

    File(".").listFiles().filter { it.isDirectory }.forEach {
        if (!File("${it.name}${sep}level.dat").exists()) return@forEach

        val world = server.getWorld(it.name)
        if ((loaded && world == null) || (!loaded && world != null)) return@forEach

        val worldFormat = when {
            File("${it.name}${sep}justworlds.yml").exists() -> WorldFormat.JUSTWORLDS
            File("${it.name}${sep}world.jw").exists() -> WorldFormat.JUSTWORLDS_OLD

            else -> WorldFormat.OTHER
        }

        worlds[it.name] = worldFormat
    }

    val justWorldsWorlds = worlds.filter { it.value == WorldFormat.JUSTWORLDS }
    val justWorldsOldWorlds = worlds.filter { it.value == WorldFormat.JUSTWORLDS_OLD }
    val otherWorlds = worlds.filter { it.value == WorldFormat.OTHER }

    val newWorlds = mutableMapOf<String, WorldFormat>()
    newWorlds.putAll(justWorldsWorlds)
    newWorlds.putAll(justWorldsOldWorlds)
    newWorlds.putAll(otherWorlds)

    return newWorlds
}

fun getAllWorlds(server: Server): Map<String, WorldFormat> {
    val loadedWorlds = getWorlds(true, server)
    val unloadedWorlds = getWorlds(false, server)

    val worlds = mutableMapOf<String, WorldFormat>()
    worlds.putAll(loadedWorlds)
    worlds.putAll(unloadedWorlds)

    return worlds
}

fun setWorldSpawn(name: String, location: Location, server: Server) {
    val x = location.x
    val y = location.y
    val z = location.z
    val yaw = location.yaw
    val pitch = location.pitch

    val world = server.getWorld(name)!!
    val configFile = getWorldConfigFile(world.name)
    val config = YamlConfiguration.loadConfiguration(configFile)

    config.set("spawn.x", x)
    config.set("spawn.y", y)
    config.set("spawn.z", z)
    config.set("spawn.yaw", yaw)
    config.set("spawn.pitch", pitch)
    config.save(configFile)
}

fun setWorldPrefix(name: String, prefix: String) {
    val configFile = getWorldConfigFile(name)
    val config = YamlConfiguration.loadConfiguration(configFile)

    config.set("prefix", prefix)
    config.save(configFile)
}

fun getWorldPrefix(name: String): String {
    val configFile = getWorldConfigFile(name)
    val config = YamlConfiguration.loadConfiguration(configFile)

    return config.getString("prefix") ?: name
}

fun getWorldConfigFile(name: String): File = File("$name${sep}justworlds.yml")