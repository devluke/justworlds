package net.lukepchambers.justworlds.listener

import net.lukepchambers.justworlds.JWPlugin
import net.lukepchambers.justworlds.JWWorldManager
import net.lukepchambers.justworlds.util.getWorldConfigFile
import net.lukepchambers.justworlds.util.msg
import org.bukkit.WorldCreator
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.world.WorldLoadEvent

class WorldLoadListener(private val plugin: JWPlugin): Listener {
    private val worldManager = JWWorldManager(plugin.server)

    @EventHandler
    fun onWorldLoad(e: WorldLoadEvent) {
        val configFile = getWorldConfigFile(e.world.name)
        if (!configFile.exists()) {
            plugin.server.consoleSender.msg("Found unmigrated world &e${e.world.name}&f; migrating")

            val worldCreator = WorldCreator(e.world.name)
            worldCreator.environment(e.world.environment)
            worldCreator.generateStructures(e.world.canGenerateStructures())
            worldCreator.generator(e.world.generator)
            worldCreator.seed(e.world.seed)
            worldCreator.type(e.world.worldType!!)

            worldManager.createWorld(worldCreator)
            worldManager.configWorld(e.world.name)
        }
    }
}