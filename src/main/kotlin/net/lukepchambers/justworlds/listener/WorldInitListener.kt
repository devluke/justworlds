package net.lukepchambers.justworlds.listener

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.world.WorldInitEvent
import java.io.File

class WorldInitListener: Listener {
    @EventHandler
    fun onWorldInit(e: WorldInitEvent) {
        e.world.keepSpawnInMemory = false
        if (!File("${e.world.worldFolder}${File.separator}level.dat").exists()) e.world.save()
    }
}