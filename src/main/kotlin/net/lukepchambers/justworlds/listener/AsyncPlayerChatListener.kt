package net.lukepchambers.justworlds.listener

import net.lukepchambers.justworlds.util.getWorldPrefix
import org.bukkit.ChatColor
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.AsyncPlayerChatEvent

class AsyncPlayerChatListener: Listener {
    @EventHandler
    fun onAsyncPlayerChat(e: AsyncPlayerChatEvent) {
        val world = e.player.world
        val prefix = getWorldPrefix(world.name)

        val rawFormat = "&f[$prefix&f]&r"
        val format = ChatColor.translateAlternateColorCodes('&', rawFormat)

        e.format = "$format${e.format}"
    }
}