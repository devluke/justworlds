package net.lukepchambers.justworlds.command

import net.lukepchambers.justworlds.JWCommand
import net.lukepchambers.justworlds.JWPlugin
import net.lukepchambers.justworlds.JWWorldManager
import net.lukepchambers.justworlds.util.SenderType
import net.lukepchambers.justworlds.util.msg
import net.lukepchambers.justworlds.util.setWorldSpawn
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class SetspawnCommand(private val plugin: JWPlugin): JWCommand() {
    override val name = "setspawn"
    override val description = "Set this world's spawn"
    override val usage = ""
    override val argRange = 0..0
    override val senderType = SenderType.PLAYER

    private val worldManager = JWWorldManager(plugin.server)

    override fun runCommand(sender: CommandSender, args: List<String>): Boolean {
        val player = sender as Player
        val location = player.location
        val world = player.world

        setWorldSpawn(world.name, location, plugin.server)
        worldManager.configWorld(world.name)

        sender.msg("Successfully set spawn for this world.")

        return true
    }
}