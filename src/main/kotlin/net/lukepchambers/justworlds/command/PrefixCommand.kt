package net.lukepchambers.justworlds.command

import net.lukepchambers.justworlds.JWCommand
import net.lukepchambers.justworlds.util.SenderType
import net.lukepchambers.justworlds.util.msg
import net.lukepchambers.justworlds.util.setWorldPrefix
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class PrefixCommand: JWCommand() {
    override val name = "prefix"
    override val description = "Manage this world's prefix"
    override val usage = "<set|clear> [new]"
    override val argRange = 1..2
    override val senderType = SenderType.PLAYER

    override fun runCommand(sender: CommandSender, args: List<String>): Boolean {
        val argTest1 = !listOf("set", "clear").contains(args[0])
        val argTest2 = args[0] == "set" && args.size != 2
        val argTest3 = args[0] == "clear" && args.size != 1

        if (argTest1 || argTest2 || argTest3) {
            invalidArgs(sender)
            return true
        }

        val player = sender as Player
        val world = player.world

        val prefix = (if (args[0] == "set") args[1] else world.name)
                .replace('_', ' ')
                .replace("\\ ", "_")

        setWorldPrefix(world.name, prefix)

        sender.msg("Successfully set this world's prefix to &e$prefix&f.")

        return true
    }
}