package net.lukepchambers.justworlds.command

import net.lukepchambers.justworlds.JWCommand
import net.lukepchambers.justworlds.JWPlugin
import net.lukepchambers.justworlds.util.SenderType
import net.lukepchambers.justworlds.util.msg
import net.lukepchambers.justworlds.util.worldExists
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class TpCommand(private val plugin: JWPlugin): JWCommand() {
    override val name = "tp"
    override val description = "Teleport to a world"
    override val usage = "<world>"
    override val argRange = 1..1
    override val senderType = SenderType.PLAYER

    override fun runCommand(sender: CommandSender, args: List<String>): Boolean {
        if (!worldExists(args[0], plugin.server)) {
            sender.msg("&cThat world does not exist. Type &e/jw list&c for a list of worlds.")
            return true
        }

        val world = plugin.server.getWorld(args[0])!!
        val player = sender as Player

        player.teleport(world.spawnLocation)

        sender.msg("You have been teleported to &e${world.name}&f.")

        return true
    }
}