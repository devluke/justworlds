package net.lukepchambers.justworlds.command

import net.lukepchambers.justworlds.JWCommand
import net.lukepchambers.justworlds.JWPlugin
import net.lukepchambers.justworlds.util.SenderType
import net.lukepchambers.justworlds.util.msg
import org.bukkit.command.CommandSender
import java.lang.Math.ceil

class HelpCommand(private val plugin: JWPlugin): JWCommand() {
    override val name = "help"
    override val description = "Display a help menu"
    override val usage = "[page]"
    override val argRange = 0..1
    override val senderType = SenderType.ANY

    override fun runCommand(sender: CommandSender, args: List<String>): Boolean {
        var page = 1
        if (args.isNotEmpty()) {
            if (args[0].toIntOrNull() == null) {
                invalidArgs(sender)
                return true
            }

            page = args[0].toInt()
        }

        val cmdMap = plugin.commandMap
        val perPage = 8

        val maxPages = ceil(cmdMap.size.toDouble() / perPage).toInt()
        if (page > maxPages) page = maxPages

        val firstIndex = (page - 1) * perPage
        val secondIndex = (page * perPage) - 1
        val indexRange = firstIndex..secondIndex

        val commands = cmdMap
                .filter { cmdMap.keys.indexOf(it.key) in indexRange }
                .filter { sender.hasPermission("justworlds.cmd.${it.value.name}") }

        sender.msg("List of JustWorlds commands:")
        sender.msg("Page &e$page&f/&e$maxPages")

        commands.forEach {
            sender.msg("&e/jw ${it.key} &3&l»&r &f${it.value.description}")
        }

        return true
    }
}