package net.lukepchambers.justworlds.command

import net.lukepchambers.justworlds.JWCommand
import net.lukepchambers.justworlds.util.SenderType
import net.lukepchambers.justworlds.util.msg
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class SpawnCommand: JWCommand() {
    override val name = "spawn"
    override val description = "Teleport to this world's spawn"
    override val usage = ""
    override val argRange = 0..0
    override val senderType = SenderType.PLAYER

    override fun runCommand(sender: CommandSender, args: List<String>): Boolean {
        val player = sender as Player
        val location = player.world.spawnLocation
        player.teleport(location)

        sender.msg("Teleported to this world's spawn.")
        return true
    }
}