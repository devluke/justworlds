package net.lukepchambers.justworlds.command

import net.lukepchambers.justworlds.JWCommand
import net.lukepchambers.justworlds.JWPlugin
import net.lukepchambers.justworlds.JWWorldManager
import net.lukepchambers.justworlds.util.SenderType
import net.lukepchambers.justworlds.util.msg
import net.lukepchambers.justworlds.util.worldExists
import org.bukkit.World
import org.bukkit.WorldCreator
import org.bukkit.WorldType
import org.bukkit.command.CommandSender
import java.io.File

class CreateCommand(private val plugin: JWPlugin): JWCommand() {
    override val name = "create"
    override val description = "Create a world"
    override val usage = "<name> [e:<normal|nether|the_end>] [a:<true|false>] [g:<generator>] [s:<seed>] [t:<normal|flat|large_biomes|amplified>]"
    override val argRange = 1..6
    override val senderType = SenderType.ANY

    private val worldManager = JWWorldManager(plugin.server)

    override fun runCommand(sender: CommandSender, args: List<String>): Boolean {
        if (worldExists(args[0], plugin.server)) {
            sender.msg("&cA world with that name already exists.")
            return true
        }

        val worldFile = File(args[0])
        if (worldFile.exists()) {
            sender.msg("&cA file with that name already exists.")
            return true
        }

        val worldCreator = WorldCreator(args[0])

        val specialArgs = args.drop(1)
        specialArgs.forEach {
            if (!it.contains(':')) {
                invalidArgs(sender)
                return true
            }

            val property = it.substringBefore(':')
            val value = it.substringAfter(':')
            val upperValue = value.toUpperCase()

            when (property) {
                "e" -> {
                    if (!listOf("NORMAL", "NETHER", "THE_END").contains(upperValue)) {
                        invalidArgs(sender)
                        return true
                    }

                    worldCreator.environment(World.Environment.valueOf(upperValue))
                }

                "a" -> {
                    if (!listOf("TRUE", "FALSE").contains(upperValue)) {
                        invalidArgs(sender)
                        return true
                    }

                    worldCreator.generateStructures(value.toBoolean())
                }

                "g" -> {
                    worldCreator.generator(value)
                }

                "s" -> {
                    val seed = value.toLongOrNull() ?: value.hashCode().toLong()
                    worldCreator.seed(seed)
                }

                "t" -> {
                    if (!listOf("NORMAL", "FLAT", "LARGE_BIOMES", "AMPLIFIED").contains(upperValue)) {
                        invalidArgs(sender)
                        return true
                    }

                    worldCreator.type(WorldType.valueOf(upperValue))
                }

                else -> {
                    invalidArgs(sender)
                    return true
                }
            }
        }

        worldManager.createWorld(worldCreator)
        worldManager.loadWorld(worldCreator.name())

        sender.msg("Successfully created world &e${worldCreator.name()}&f.")

        return true
    }
}