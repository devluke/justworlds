package net.lukepchambers.justworlds.command

import net.lukepchambers.justworlds.JWCommand
import net.lukepchambers.justworlds.JWPlugin
import net.lukepchambers.justworlds.util.SenderType
import net.lukepchambers.justworlds.util.getAllWorlds
import net.lukepchambers.justworlds.util.msg
import net.lukepchambers.justworlds.util.worldExists
import org.bukkit.command.CommandSender

class ListCommand(private val plugin: JWPlugin): JWCommand() {
    override val name = "list"
    override val description = "Get a list of worlds"
    override val usage = "[page]"
    override val argRange = 0..1
    override val senderType = SenderType.ANY

    override fun runCommand(sender: CommandSender, args: List<String>): Boolean {
        var page = 1
        if (args.isNotEmpty()) {
            if (args[0].toIntOrNull() == null) {
                invalidArgs(sender)
                return true
            }

            page = args[0].toInt()
        }

        val worldMap = getAllWorlds(plugin.server)
        val perPage = 8

        val maxPages = Math.ceil(worldMap.size.toDouble() / perPage).toInt()
        if (page > maxPages) page = maxPages

        val firstIndex = (page - 1) * perPage
        val secondIndex = (page * perPage) - 1
        val indexRange = firstIndex..secondIndex

        val worlds = worldMap.filter { worldMap.keys.indexOf(it.key) in indexRange }

        sender.msg("List of worlds:")
        sender.msg("Page &e$page&f/&e$maxPages")

        worlds.forEach {
            val nameColor = if (worldExists(it.key, plugin.server)) "&e" else "&7"
            sender.msg("$nameColor${it.key}&r &3&l»&r &f${it.value.formatName}")
        }

        return true
    }
}