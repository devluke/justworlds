package net.lukepchambers.justworlds

import net.lukepchambers.justworlds.command.*
import net.lukepchambers.justworlds.listener.AsyncPlayerChatListener
import net.lukepchambers.justworlds.listener.WorldInitListener
import net.lukepchambers.justworlds.listener.WorldLoadListener
import net.lukepchambers.justworlds.util.WorldFormat
import net.lukepchambers.justworlds.util.getWorlds
import net.lukepchambers.justworlds.util.msg
import org.bstats.bukkit.Metrics
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.plugin.java.JavaPlugin

class JWPlugin: JavaPlugin() {
    private lateinit var worldManager: JWWorldManager

    val commandMap = mutableMapOf(
            "create" to CreateCommand(this),
            "help" to HelpCommand(this),
            "list" to ListCommand(this),
            "prefix" to PrefixCommand(),
            "setspawn" to SetspawnCommand(this),
            "spawn" to SpawnCommand(),
            "tp" to TpCommand(this)
    )

    override fun onEnable() {
        @Suppress("UNUSED_VARIABLE") val metrics = Metrics(this)

        server.pluginManager.registerEvents(AsyncPlayerChatListener(), this)
        server.pluginManager.registerEvents(WorldInitListener(), this)
        server.pluginManager.registerEvents(WorldLoadListener(this), this)

        worldManager = JWWorldManager(this.server)

        getWorlds(false, server).filterValues { it == WorldFormat.JUSTWORLDS }.forEach {
            worldManager.loadWorld(it.key)
        }

        server.consoleSender.msg("Enabled JustWorlds version ${description.version}")
    }

    override fun onDisable() {
        server.consoleSender.msg("Disabled JustWorlds")
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
        if (command.name == "justworlds") {
            val finalArgs = args.toMutableList()

            val helpShortcut = args.isEmpty() || args[0].toIntOrNull() != null
            if (helpShortcut) finalArgs.add(0, "help")

            if (!commandMap.containsKey(finalArgs[0]) || (helpShortcut && args.size !in 0..1)) {
                sender.msg("&cInvalid subcommand. Type &e/jw help &cfor help.")
                return true
            }

            val cmd = commandMap[finalArgs[0]]!!

            val permission = "justworlds.cmd.${cmd.name}"
            if (!sender.hasPermission(permission)) {
                sender.msg("&cYou do not have permission to run this command.")
                sender.msg("&cAsk an admin to grant you the permission &e$permission&c.")
                return true
            }

            if (!cmd.senderType.typeClass.isInstance(sender)) {
                sender.msg("&cYou can only run this command as &e${cmd.senderType.typeName}&c.")
                return true
            }

            val newArgs = args.drop(1)

            if (newArgs.size !in cmd.argRange) {
                cmd.invalidArgs(sender)
                return true
            }

            return cmd.runCommand(sender, newArgs)
        }

        return false
    }

    override fun onTabComplete(sender: CommandSender, command: Command, alias: String, args: Array<String>): List<String> {
        if (args.size == 1) return commandMap.keys.toList()
        return listOf()
    }
}