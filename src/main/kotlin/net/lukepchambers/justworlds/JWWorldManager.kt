package net.lukepchambers.justworlds

import net.lukepchambers.justworlds.util.getWorldConfigFile
import net.lukepchambers.justworlds.util.setWorldPrefix
import net.lukepchambers.justworlds.util.setWorldSpawn
import org.bukkit.*
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File

class JWWorldManager(private val server: Server) {
    fun createWorld(worldCreator: WorldCreator) {
        val name = worldCreator.name()
        val dirFile = File(name)
        dirFile.mkdirs()

        val configFile = getWorldConfigFile(name)
        configFile.createNewFile()

        val config = YamlConfiguration.loadConfiguration(configFile)
        config.set("world.environment", worldCreator.environment().toString())
        config.set("world.structures", worldCreator.generateStructures())
        config.set("world.generator", worldCreator.generator())
        config.set("world.seed", worldCreator.seed())
        config.set("world.type", worldCreator.type().toString())

        config.save(configFile)
    }

    fun loadWorld(name: String) {
        val configFile = getWorldConfigFile(name)
        val config = YamlConfiguration.loadConfiguration(configFile)

        val worldCreator = WorldCreator(name)

        worldCreator.environment(World.Environment.valueOf(config.getString("world.environment")!!))
        worldCreator.generateStructures(config.getBoolean("world.structures"))
        worldCreator.generator(config.getString("world.generator"))
        worldCreator.seed(config.getLong("world.seed"))
        worldCreator.type(WorldType.valueOf(config.getString("world.type")!!))

        worldCreator.createWorld()
        configWorld(worldCreator.name())
    }

    fun configWorld(name: String) {
        val configFile = getWorldConfigFile(name)
        val config = YamlConfiguration.loadConfiguration(configFile)

        val world = server.getWorld(name)!!

        if (!config.contains("spawn")) setWorldSpawn(world.name, world.spawnLocation, server)
        if (!config.contains("prefix")) setWorldPrefix(world.name, world.name)

        val x = config.getDouble("spawn.x")
        val y = config.getDouble("spawn.y")
        val z = config.getDouble("spawn.z")
        val yaw = config.getDouble("spawn.yaw")
        val pitch = config.getDouble("spawn.pitch")

        val location = Location(world, x, y, z, yaw.toFloat(), pitch.toFloat())
        world.spawnLocation = location
    }
}